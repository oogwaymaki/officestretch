//
//  ScheduledEvents.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/3/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScheduledEvent.h"

@interface ScheduledEvents : NSObject

@property (strong,atomic) NSMutableArray *scheduledEvents;;

@end
