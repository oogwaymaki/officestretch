//
//  EndTimeSelection.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/29/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EndTimeSelection : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;
- (IBAction)cancelEntry:(id)sender;
- (IBAction)saveEntry:(id)sender;


@end
