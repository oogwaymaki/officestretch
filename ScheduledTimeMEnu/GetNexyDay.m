//
//  GetNexyDay.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/3/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "GetNexyDay.h"

@implementation GetNexyDay

@synthesize hour;
@synthesize minute;
@synthesize dayOfWeek;
@synthesize ampm;

enum Weeks {
    SUN = 1,
    MON,
    TUE,
    WED,
    THU,
    FRI,
    SAT
};
- (NSInteger) getDay
{
    if ([dayOfWeek isEqualToString:@"Mon"])
        return MON;
    else if ([dayOfWeek isEqualToString:@"Tue"])
        return  TUE;
    else if ([dayOfWeek isEqualToString:@"Wed"])
        return WED;
    else if ([dayOfWeek isEqualToString:@"Thu"])
        return THU;
    else if ([dayOfWeek isEqualToString:@"Fri"])
        return FRI;
    else if ([dayOfWeek isEqualToString:@"Sat"])
        return SAT;
    else if ([dayOfWeek isEqualToString:@"Sun"])
        return THU;
    return -1;
}
- (NSInteger)getAddHour
{
    if ([self.ampm isEqualToString:@"PM"])
        return 12;
    else return 0;
}
- (NSDate *) getTheScheduledTime
{
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *date = [NSDate date];
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    NSInteger todayWeekday = [weekdayComponents weekday];
    
    NSInteger moveDays= [ self getDay ]  - todayWeekday;
    if (moveDays<=0) {
        moveDays+=7;
    }
    
    NSDateComponents *components = [NSDateComponents new];
    components.day=moveDays;
     NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    
    NSInteger addhour = [ self getAddHour ];
    if ([hour integerValue] == 12)
        hour = 0;
    NSInteger hours = [ hour integerValue ] + addhour;
    
    signed unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents *comps = [calendar components:unitFlags fromDate:newDate];
    
    [ comps  setHour:hours ];
    [ comps setMinute:[minute integerValue]];
    newDate = [gregorian dateFromComponents:comps];
    return newDate;
}
@end
