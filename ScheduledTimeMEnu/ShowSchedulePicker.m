//
//  ShowSchedulePicker.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/2/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "ShowSchedulePicker.h"
#import "GetNexyDay.h"

@implementation ShowSchedulePicker

@synthesize dayOfWeek;
@synthesize hour;
@synthesize minute;
@synthesize amPm;
@synthesize type;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- ( void ) viewWillAppear:(BOOL)animated{
   [ super viewWillAppear:animated];
    
    _pickerDay.showsSelectionIndicator = YES;
    _pickerDay.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    CGSize ps = [_pickerDay sizeThatFits: CGSizeZero];
    _pickerDay.frame = CGRectMake(0.0, 0.0, ps.width, ps.height + 100);
}
- (void)viewDidLoad {
    [super viewDidLoad];
 
    
    _pickerDay.delegate = self;
    _pickerDay.dataSource = self;
    if ([self.type isEqualToString:@"endDate"])
        self.dayOfWeek = [[NSArray alloc] initWithObjects: self.endDay,nil];
    else
    self.dayOfWeek = [[NSArray alloc] initWithObjects:
                         @"Mon", @"Tue", @"Wed",
                      @"Thu", @"Fri", @"Sat",@"Sun" ,nil];
    self.hour = [[NSArray alloc ] initWithObjects: [NSNumber  numberWithInt:1],
                 [NSNumber numberWithInt:2],
                 [NSNumber numberWithInt:3],
                 [NSNumber numberWithInt:4],
                 [NSNumber numberWithInt:5],
                 
                 [NSNumber numberWithInt:6],
                 [NSNumber numberWithInt:7],
                 
                 [NSNumber numberWithInt:8],
                 [NSNumber numberWithInt:9],
                 [NSNumber numberWithInt:10],
                 [NSNumber numberWithInt:11],
                 [NSNumber numberWithInt:12],
                 nil];
    minute = [[NSMutableArray alloc] init ];
    for(int i=0; i<60 ; i++)
    {

        NSNumber *number = [NSNumber numberWithInt:i];
        [minute addObject:number];
    }

    self.amPm = [[NSArray alloc] initWithObjects: @"AM", @"PM",nil];
    
    
#pragma mark -
#pragma mark PickerView DataSourceI
}
- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    NSInteger d = 5;
    return d;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
    {
        if (component == 0)
            return [dayOfWeek count];
        if (component == 1 )
            return [ hour  count ];
        if (component == 3)
            return [ minute count ] ;
        if (component == 2)
            return 1 ;
        if (component == 4)
            return [ amPm count ];
        
        return 0;
    }
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
        if (component == 0)
            return [ dayOfWeek objectAtIndex: row];
        if (component == 1)
            return   [ NSString stringWithFormat:@"%@", [hour objectAtIndex: row]];
        if (component ==3)
        {
            if ([(NSNumber *)[minute objectAtIndex: row ] integerValue ] < 10)
            return   [ NSString stringWithFormat:@"0%@", [minute objectAtIndex: row]];
                
            else
            return   [ NSString stringWithFormat:@"%@", [minute objectAtIndex: row]];
        }
        if (component == 2)
            return @":";
        if (component ==4)
            return [ amPm objectAtIndex: row];
    return @"not supported";
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 2)
        return 13;
    if (component == 1)
        return 40;
    if (component == 3)
        return 40;
    return 60;
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

#pragma mark -
#pragma mark PickerView Delegate
- (IBAction)selectTime:(id)sender {
  
    NSDate *date;
    GetNexyDay *getday = [[ GetNexyDay alloc ] init ];
    getday.dayOfWeek =  [self.dayOfWeek objectAtIndex:[_pickerDay selectedRowInComponent:0]];
    getday.hour =  [self.hour objectAtIndex:[_pickerDay selectedRowInComponent:1]];
    getday.minute = [ self.minute objectAtIndex:    [_pickerDay selectedRowInComponent:3]];
    getday.ampm = [ self.amPm objectAtIndex: [_pickerDay selectedRowInComponent:4]];
    date = [ getday getTheScheduledTime ];
    [self.delegate addItemViewController:self didFinishEnteringItem:date dateType:self.type endDay:getday.dayOfWeek];
}

- (IBAction)cancel:(id)sender {
    [ self.delegate ShowScheduleControllerDidCancel:self];
}
@end
