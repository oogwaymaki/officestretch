//
//  TimeMenu.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/3/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "TimeMenu.h"
#import "ScheduledEvent.h"
#import "ScheduledEvents.h"
#import "ShowSchedulePicker.h"




@implementation TimeMenu


@synthesize intervalTxt;
@synthesize endButton;
@synthesize endDay;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIControl *viewControl = (UIControl*)self.view;
    [viewControl addTarget:self action:@selector(resignKeyboard:) forControlEvents:UIControlEventTouchDown];
    endButton.enabled = NO;
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.keyboardType == UIKeyboardTypeNumberPad)
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            return NO;
    return YES;
}

- (IBAction)resignKeyboard:(id)sender {
    if ([self.intervalTxt isFirstResponder]) {
        [self.intervalTxt resignFirstResponder];
        
        // put code to run after return key pressed here...
    }
}

- (IBAction)chooeStartTime:(id)sender {
    
    
    UIStoryboard *storybaord = [UIStoryboard storyboardWithName:@"TimeStory" bundle:nil];
    
    
    ShowSchedulePicker *viewController =[storybaord instantiateViewControllerWithIdentifier:@"ShowSchedulePicker"];
    viewController.delegate = self;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:viewController];
    nc.modalPresentationStyle = UIModalPresentationPageSheet;
    UIStoryboardSegue *segue = [[ UIStoryboardSegue alloc ] initWithIdentifier:@"startDate" source:self  destination:viewController];
    [ nc prepareForSegue:segue sender: self];
  
}

- (IBAction)chooseEndTime:(id)sender {
    UIStoryboard *storybaord = [UIStoryboard storyboardWithName:@"TimeStory" bundle:nil];
    
    
    ShowSchedulePicker *viewController =[storybaord instantiateViewControllerWithIdentifier:@"ShowSchedulePicker"];
    viewController.delegate = self;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:viewController];
    nc.modalPresentationStyle = UIModalPresentationPageSheet;
    UIStoryboardSegue *segue = [[ UIStoryboardSegue alloc ] initWithIdentifier:@"endDate" source:self  destination:viewController];
    [ nc prepareForSegue:segue sender: self];
    
}
- (IBAction)saveSettings:(id)sender
{
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    ScheduledEvent *event = [[ ScheduledEvent alloc ] init ];
    event.minuteInterval = [ intervalTxt.text integerValue ];

}
- (void)addItemViewController:(ShowSchedulePicker *)controller didFinishEnteringItem:(NSDate *)item dateType:(NSString *)type endDay:(NSString *)endDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [ dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [ dateFormatter setDateStyle:NSDateFormatterFullStyle];
    
    NSString *formatString = [dateFormatter stringFromDate:item];
    
    if ([type isEqualToString:@"startDate"])
    {
        self.startDate = item;
        self.fromLabel.text = formatString;
        self.endDay = endDay;
        endButton.enabled = true;
    }
    
    if ([type isEqualToString:@"endDate"])
    {
        self.endDate = item;
        self.toLabel.text = formatString;
    }
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];

}
-(void) prepareForSegue:(UIStoryboardPopoverSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[ShowSchedulePicker class]]) {
        ShowSchedulePicker *vc = (ShowSchedulePicker *) segue.destinationViewController;
        if ([[segue identifier] isEqualToString:@"startDate"])
            vc.type = @"startDate";
        else
        {
            vc.type = @"endDate";
            vc.endDay = self.endDay;
        }
        vc.delegate = self;
    }
}
#pragma mark - SelectMaterialViewControllerDelegate methods

- (void)ShowScheduleControllerDidCancel:(ShowSchedulePicker *)controller
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
