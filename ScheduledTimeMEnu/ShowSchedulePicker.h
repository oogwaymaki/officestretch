//
//  ShowSchedulePicker.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/2/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShowSchedulePicker;

@protocol ShowSchedulePickerDelegate <NSObject>
- (void)addItemViewController:(ShowSchedulePicker *)controller didFinishEnteringItem:(NSDate *)item dateType:(NSString *) dateType endDay:(NSString *) endDay;
- (void)ShowScheduleControllerDidCancel:(ShowSchedulePicker *)controller;
@end

@interface ShowSchedulePicker : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>
{
    NSArray            *dayOfWeek;
    NSArray            *hour;
    NSMutableArray     *minute;
    NSArray            *amPm;
}
- (IBAction)selectTime:(id)sender;


@property (strong,nonatomic) IBOutlet UIButton *button;
@property (nonatomic, weak) id <ShowSchedulePickerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerDay;
@property (strong, nonatomic) NSArray *dayOfWeek;;
@property (strong, nonatomic) NSArray *hour;
@property (strong, nonatomic) NSMutableArray *minute;
@property (strong, nonatomic) NSArray *amPm;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *endDay;

@end
