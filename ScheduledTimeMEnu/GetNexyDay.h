//
//  GetNexyDay.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/3/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetNexyDay : NSObject

@property (strong,atomic) NSNumber *hour;
@property (strong,atomic) NSNumber *minute;
@property (strong,atomic) NSString *dayOfWeek;
@property (strong,atomic) NSString *ampm;
- (NSDate *) getTheScheduledTime;
@end
