//
//  ScheduledTimeMenu.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/28/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "ScheduledTimeMenu.h"
#import "OfficeStretchAppDelegate.h"

@implementation ScheduledTimeMenu

@synthesize slider;
@synthesize slidercounter;
@synthesize datepicker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (IBAction)saveSettings:(id)sender
{
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [ prefs setInteger:slider.value forKey:@"Interval" ];
    [ prefs setObject:datepicker.date forKey:@"StartDate" ];
    [ prefs synchronize ];
    
    
    OfficeStretchAppDelegate *appDelegate = (OfficeStretchAppDelegate *)[[UIApplication sharedApplication] delegate];
                                             
    appDelegate.revealController.frontViewController = appDelegate.mainController;

    
}
- (IBAction)cancelRequest:(id)sender {
    OfficeStretchAppDelegate *appDelegate = (OfficeStretchAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.revealController.frontViewController = appDelegate.mainController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    slider.value = 1;
    slider.continuous = true;
    [slider addTarget:self action:@selector(mySliderChanged:) forControlEvents:UIControlEventValueChanged];
    slidercounter.textColor = [ UIColor redColor];
    slidercounter.text = [NSString stringWithFormat:@"%d",(int) slider.value ];
    datepicker.backgroundColor = [UIColor blueColor];
    
     // Do any additional setup after loading the view from its nib.
}

- (void)mySliderChanged:(NSNotification *)notification
{
    NSString *labelString = [NSString stringWithFormat:@"%d", (int) slider.value];
    [ slidercounter setText:labelString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)slider:(id)sender {
}

@end
