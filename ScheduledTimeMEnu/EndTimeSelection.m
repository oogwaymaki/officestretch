//
//  EndTimeSelection.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/29/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "EndTimeSelection.h"
#import "OfficeStretchAppDelegate.h"

@interface EndTimeSelection ()

@end

@implementation EndTimeSelection

@synthesize datepicker;;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    datepicker.backgroundColor = [UIColor blueColor];
    
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelEntry:(id)sender {
    
    OfficeStretchAppDelegate *appDelegate = (OfficeStretchAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.revealController.frontViewController = appDelegate.mainController;

}

- (IBAction)saveEntry:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [ prefs setObject:datepicker.date forKey:@"EndDate" ];
    [ prefs synchronize ];
    
    
    OfficeStretchAppDelegate *appDelegate = (OfficeStretchAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.revealController.frontViewController = appDelegate.mainController;
}
@end
