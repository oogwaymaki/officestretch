//
//  ScheduledTimeMenu.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/28/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduledTimeMenu : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *slidercounter;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;
- (IBAction)slider:(id)sender;
- (IBAction)saveSettings:(id)sender;
- (IBAction)cancelRequest:(id)sender;

@end

