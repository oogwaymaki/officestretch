//
//  TimeMenu.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/3/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHowSchedulePicker.h"

@interface TimeMenu : UIViewController <ShowSchedulePickerDelegate>


@property (strong, nonatomic) IBOutlet UILabel *intervalLabl;
@property (strong, nonatomic) IBOutlet UITextField *intervalTxt;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) IBOutlet UILabel *toLabel;
@property (strong, nonatomic) IBOutlet UILabel *fromLabel;
@property (strong, nonatomic) IBOutlet UIButton *endButton;
@property (strong, nonatomic) NSString *endDay;

- (IBAction)resignKeyboard:(UITextField *)sender;
- (IBAction)chooeStartTime:(id)sender;
- (IBAction)chooseEndTime:(id)sender;

@end
