//
//  ScheduledWeekDay.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 11/2/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "ScheduledWeekDay.h"

@interface ScheduledWeekDay ()

@end

@implementation ScheduledWeekDay

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
