//
//  ScheduledEvent.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/28/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "ScheduledEvent.h"

@implementation ScheduledEvent

@synthesize scheduledStart;
@synthesize scheduledEnd;
@synthesize message;
@synthesize minuteInterval;
@synthesize showExerciseType;
@synthesize eventName;
@end
