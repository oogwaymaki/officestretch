//
//  LevelExercises.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StretchSingleRecord.h"
#import "JSONModel.h"

@protocol StretchSingleRecord
@end

@interface LevelExercises : JSONModel
@property (assign, nonatomic) int level;
@property (strong, nonatomic) NSArray <StretchSingleRecord>* type;

@end
