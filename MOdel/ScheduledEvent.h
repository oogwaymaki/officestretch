//
//  ScheduledEvent.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/28/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScheduledEvent : NSObject

@property (strong,atomic) NSString *eventName;
@property (strong,atomic) NSDate *scheduledStart;
@property (assign,nonatomic) NSInteger minuteInterval;
@property (strong,atomic) NSString *showExerciseType;
@property (strong,atomic) NSString *message;
@property (strong,atomic) NSString *scheduledEnd;

@end
