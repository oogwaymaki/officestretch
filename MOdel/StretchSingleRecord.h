    //
//  StretchSingleRecord.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface StretchSingleRecord : JSONModel
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *method;
@end
