//
//  NotificationHandle.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/28/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "NotificationHandle.h"
#import "ScheduledEvent.h"


@implementation NotificationHandle

@synthesize isrunning;

static NotificationHandle *notification;
+ (NotificationHandle*)sharedInstance
{
    if (notification == nil) {
        notification = [[super allocWithZone:NULL] init];
    }
    return notification;
}

- (void)scheduleNotificationWithItem:(ScheduledEvent *)item
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    NSDate *itemDate = item.scheduledStart;
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    int minuteInterval = item.minuteInterval;
    localNotif.fireDate = [ itemDate dateByAddingTimeInterval:60*minuteInterval];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    localNotif.alertBody = @"Time to Exercise";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    NSDictionary *infoDict = [ NSDictionary dictionaryWithObject:item.message forKey:@"message"];
    localNotif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

@end
