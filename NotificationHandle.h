//
//  NotificationHandle.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/28/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScheduledEvent.h"
@interface NotificationHandle : NSObject
- (void)scheduleNotificationWithItem:(ScheduledEvent *)item;
+ (NotificationHandle*)sharedInstance;
@property(assign) BOOL isrunning;

@end
