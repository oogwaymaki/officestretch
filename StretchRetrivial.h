//
//  StretchRetrivial.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StretchRetrivial : NSObject
- (NSString *) getStretchFileContents:(NSInteger)level typeExercise:(NSString *)typeExercise;


@end
