//
//  StretchRetrivial.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "StretchRetrivial.h"

@implementation StretchRetrivial


- (NSString *) getStretchFileContents:(NSInteger)level typeExercise:(NSString *)typeExercise
{
    NSString* path = [[NSBundle mainBundle] pathForResource: @"stretches" ofType: @"json"];
    NSString *data = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return data;
}
@end