//
//  receviedNotification.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/30/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "ReceviedNotification.h"
#import "ScheduledEvent.h"
#import "NotificationHandle.h"

@implementation ReceviedNotification
static ReceviedNotification *objectReceived;


+ (ReceviedNotification *) sharedInstance

{
    if (objectReceived == nil) {
        objectReceived = [[super allocWithZone:NULL] init];
    }
    return objectReceived;
}

- (void)receivedNotificationAction
{
    NSDate *now = [NSDate date];
    ScheduledEvent *event = [[ ScheduledEvent alloc ] init ];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger idx = [ prefs integerForKey:@"counter"];
    idx = idx + 1;
    [ prefs setInteger:idx forKey: @"counter" ];
    [ prefs synchronize];
    event.scheduledStart = now;
    
    // getting an NSInteger
    event.minuteInterval = [prefs integerForKey:@"Interval"];
    
    // getting an Float
    event.message = [NSString stringWithFormat:@"%d",idx ];
    NotificationHandle *handle = [ NotificationHandle sharedInstance ] ;
    [ handle scheduleNotificationWithItem:event];
    
}

@end
