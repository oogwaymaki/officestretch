//
//  JsonParser.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "JsonParser.h"
#import "JSONModelLib.h"
#import "LevelExercises.h"
@implementation JsonParser
    
    
- (LevelExercises *) fetchedData:(NSString *)responseData {
    //parse out the json data

    NSError* err = nil;
    LevelExercises *lexercises = [[LevelExercises alloc] initWithString:responseData error:&err];

    return lexercises;
}
@end
