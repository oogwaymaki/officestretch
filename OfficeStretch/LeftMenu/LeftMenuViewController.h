//
//  LeftMenuViewController.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UITableViewController

@end
