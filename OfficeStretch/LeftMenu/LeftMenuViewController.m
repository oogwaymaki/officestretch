//
//  LeftMenuViewController.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "OfficeStretchAppDelegate.h"
#import "ScheduledTimeMenu.h"
#import "EndTimeSelection.h"
#import "NotificationHandle.h"
#import "scheduledEvent.h"
#import "ShowSchedulePicker.h"

@interface LeftMenuViewController ()

@property (nonatomic, strong) NSArray* contents;

@end

@implementation LeftMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.contents = @[ @"Select Start Time", @"Select End Time",@"Start Notification",@"Cancel Notification",@"Select Level Of Stretches" ,@"Select Notification Tune"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.contents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle =  cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    NSInteger idx = indexPath.row;
    NSString *name = [self.contents objectAtIndex:idx];
    cell.textLabel.text = name;
    cell.backgroundColor = [ UIColor blackColor];
    cell.textLabel.textColor = [UIColor redColor];
        

    
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void) hideLeftController
{
    OfficeStretchAppDelegate *appDelegate = (OfficeStretchAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.revealController.focusedController == appDelegate.revealController.leftViewController)
    {
        [appDelegate.revealController showViewController:appDelegate.revealController.frontViewController];
    }

}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    OfficeStretchAppDelegate *appDelegate = (OfficeStretchAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSInteger idx = indexPath.row;
    NSString *name = [self.contents objectAtIndex:idx];
    
    if ( [ name isEqualToString:@"Select End Time" ])
    {
        
        UIStoryboard *storybaord = [UIStoryboard storyboardWithName:@"TimeStory" bundle:nil];

        
        ShowSchedulePicker *frontController =[storybaord instantiateViewControllerWithIdentifier:@"TimeMenu"];
        
        appDelegate.revealController.frontViewController = frontController;
        [ self hideLeftController];

    }
    if ( [ name isEqualToString:@"Select Start Time" ] )
    {
        NotificationHandle *handle = [ NotificationHandle sharedInstance];
        if (!handle.isrunning)
        {
        ScheduledTimeMenu *frontController = [[ ScheduledTimeMenu alloc ] init ];
        frontController.view.backgroundColor = [UIColor blackColor];
        
        appDelegate.revealController.frontViewController = frontController;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification Started"
                                                            message:@"Notification already started."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

        }
        [ self hideLeftController];

    }
      if ( [ name isEqualToString:@"Start Notification" ] )    {
          NotificationHandle *handle = [ NotificationHandle sharedInstance];
        
          ScheduledEvent *event = [[ ScheduledEvent alloc ] init ];
           NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
          [ prefs synchronize];
          NSInteger idx = [ prefs integerForKey:@"counter"];
           // getting an NSString
           event.scheduledStart = (NSDate *)[prefs objectForKey:@"StartDate"];
           
           // getting an NSInteger
           event.minuteInterval = [prefs integerForKey:@"Interval"];
           
           // getting an Float
          event.message = [NSString stringWithFormat:@"%d",idx ];
         
          if (![ handle isrunning])
          {
           [ handle scheduleNotificationWithItem:event];
              handle.isrunning = true;
          }
          else
          {
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification Started"
                                                              message:@"Notification already started."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
              [alert show];
          }
          [ self hideLeftController];
      }
    if ( [ name isEqualToString:@"Cancel Notification"])
    {
        
        NotificationHandle *handle = [ NotificationHandle sharedInstance];
        handle.isrunning = false;
       [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [ self hideLeftController];
    }
}

@end
