//
//  OfficeStretchAppDelegate.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKRevealController.h"
#import "OfficeStretchViewController.h"

@interface OfficeStretchAppDelegate : UIResponder <UIApplicationDelegate,PKRevealing>

{
        IBOutlet OfficeStretchViewController *controller;
    
    
}
@property (nonatomic) UIStoryboard* storyboard;
@property (nonatomic, strong, readwrite) PKRevealController *revealController;
@property (nonatomic, strong,readwrite) OfficeStretchViewController *mainController;
@property (strong, nonatomic) UIWindow *window;

@end
