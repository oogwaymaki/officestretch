//
//  OfficeStretchViewController.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "OfficeStretchViewController.h"
#import "StretchRetrivial.h"
#import "JsonParser.h"
#import "StretchSingleRecord.h"
@implementation OfficeStretchViewController

@synthesize type;
@synthesize instructions;
NSInteger counter;
-(void)createScroll{

}

- (void)viewDidLoad
{
    [super viewDidLoad];
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     NSInteger idx =  [ prefs integerForKey:@"counter"];
   
    if ( idx == 0)
    {
        idx = 1;
    [ prefs setInteger:1 forKey:@"counter"];
    [prefs synchronize];
    }
    
    
    StretchRetrivial *retrievel = [[ StretchRetrivial alloc ] init];
    NSInteger num = 1;
    JsonParser *parser = [[ JsonParser alloc ] init ];
    NSString *json = [ retrievel getStretchFileContents:num typeExercise:@"Blah" ];
    LevelExercises *lexercises = [ parser fetchedData:json ];
    NSArray *exercises = lexercises.type;
    if (idx >= ([ exercises count ]-1))
    {
        idx = [ exercises count ]-1;
         [ prefs setInteger:1 forKey:@"counter"];
        [ prefs synchronize ];
        
    }
    StretchSingleRecord *exercise = (StretchSingleRecord *) [exercises objectAtIndex:idx ];
    type.textColor = [UIColor redColor];
    type.text = exercise.name;
    instructions.text = exercise.method;
    instructions.textColor = [UIColor redColor];
  
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
