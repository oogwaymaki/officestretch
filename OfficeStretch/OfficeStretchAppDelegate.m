//
//  OfficeStretchAppDelegate.m
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/27/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import "OfficeStretchAppDelegate.h"
#import "LeftMenuViewController.h"
#import "OfficeStretchViewController.h"
#import "scheduledEvent.h"
#import "ReceviedNotification.h"

@implementation OfficeStretchAppDelegate

@synthesize mainController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UILocalNotification *localNotif =
    [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif) {

        
    }
    
    

    self.storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Step 1: Create your controllers.
    OfficeStretchViewController *frontViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OfficeStretch"];
    mainController = frontViewController;
    frontViewController.view.backgroundColor = [UIColor blackColor];
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    // Step 2: Instantiate.
    self.revealController = [PKRevealController revealControllerWithFrontViewController:frontNavigationController leftViewController:[self leftViewController]];
    // Step 3: Configure.
    self.revealController.delegate = self;
    self.revealController.animationDuration = 0.25;
    
    // Step 4: Apply.
    self.window.rootViewController = self.revealController ;
    [self.window makeKeyAndVisible];
    
    return YES;}
- (UIViewController *)leftViewController
{

    LeftMenuViewController *leftViewController = [[ LeftMenuViewController alloc ] init];
     leftViewController.view.backgroundColor = [UIColor blackColor];
    return leftViewController;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    UIDevice* device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)])
        backgroundSupported = device.multitaskingSupported;

}
void isMultitaskingsupported()
{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}
- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif
{
    if(app.applicationState == UIApplicationStateActive ) {
         [self _showAlert:@"Do Exercices" withTitle:@"View Exercies"];
    }
    
    app.applicationIconBadgeNumber = app.applicationIconBadgeNumber - 1;
    ReceviedNotification *notification = [ ReceviedNotification sharedInstance];
    [ notification receivedNotificationAction];
    [ self.revealController.frontViewController viewDidLoad];
    
    

}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) _showAlert:(NSString*)pushmessage withTitle:(NSString*)title
{
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:pushmessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

@end
