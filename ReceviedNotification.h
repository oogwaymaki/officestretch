//
//  receviedNotification.h
//  OfficeStretch
//
//  Created by Dave Germiquet on 10/30/2013.
//  Copyright (c) 2013 Dave Germiquet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceviedNotification : NSObject

+ (ReceviedNotification *) sharedInstance;
- (void)receivedNotificationAction;
@end
